from flask import Flask
from flask import send_from_directory

app = Flask(__name__)

from flask import request

@app.route('/login', methods=['GET', 'POST'])
def parse_request():
    print(request.form.get('name'))
    return "<p>登录成功</p>"
@app.route("/hi")
def hello_world():
    return "<p>Hello, World!</p>"
@app.route('/web/<path:path>')
def static_file(path):
    return send_from_directory('web', path)